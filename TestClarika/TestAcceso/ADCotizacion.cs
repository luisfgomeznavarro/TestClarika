﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestModel;

namespace TestAcceso
{
    public class ADCotizacion
    {
        private Context bd = new Context();

        public List<mCotizacion> GetCotizaciones(mCotizacion Cotizacion)
        {
            try
            {
                List <mCotizacion> Cotizaciones  = bd.Cotizaciones.ToList();
                if (Cotizacion != null)
                {
                    if (Cotizacion.Activa)
                    {
                        Cotizaciones =Cotizaciones.Where(x => x.Activa).ToList();
                    }else
                    {
                        Cotizaciones = Cotizaciones.Where(x => !x.Activa).ToList();
                    }

                    if (!String.IsNullOrEmpty(Cotizacion.Cliente))
                    {
                        Cotizaciones = Cotizaciones.Where(x => x.Cliente.Contains(Cotizacion.Cliente)).ToList();
                    }

                    if (!String.IsNullOrEmpty(Cotizacion.NumeroPoliza))
                    {
                        Cotizaciones = Cotizaciones.Where(x => x.Cliente.Contains(Cotizacion.NumeroPoliza)).ToList();
                    }
                }

                return Cotizaciones;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void InsertCotizacion(mCotizacion model)
        {
            try
            {
                bd.Cotizaciones.Add(model);
                bd.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
