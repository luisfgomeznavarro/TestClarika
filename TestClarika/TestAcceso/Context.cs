﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestModel;

namespace TestAcceso
{
    public class Context : DbContext
    {
        public Context() : base("Conection") { }

        public DbSet<mCotizacion> Cotizaciones { get; set; }
    }
}
