﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestModel;

namespace TestWeb.Controllers
{
    public class HomeController : Controller
    {
        private wcfServicio.ServicioClient _svc = new wcfServicio.ServicioClient();
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ListCotizaciones()
        {
            List<mCotizacion> Cotizaciones = new List<mCotizacion>();
            try
            {
                Cotizaciones = _svc.GetCotizaciones(null);
                return PartialView("_ListCotizaciones", Cotizaciones);
            }
            catch (Exception)
            {
                ViewBag.Error = "Disculpe, pero ha ocurrido un error cargando las cotizaciones";
                return PartialView("_ListCotizaciones", Cotizaciones);
            }
        }

        public PartialViewResult NewCotizacion()
        {
            mCotizacion NewCotizacion = new mCotizacion();
            List<string> FormaPago = new List<string>();
            #region Formas de pago
            FormaPago.Add("Efectivo");
            FormaPago.Add("Tarjeta");
            FormaPago.Add("Debito");
            #endregion

            ViewBag.FormaPago = new SelectList(FormaPago, "");

            NewCotizacion.FechaVencimiento = DateTime.Today.AddYears(1);
            NewCotizacion.FechaCotizacion = DateTime.Today;
            NewCotizacion.NumeroPoliza = Guid.NewGuid().ToString();

            return PartialView("_NewCotizacion", NewCotizacion);
        }


        public PartialViewResult FiltroCotizacion()
        {
            return PartialView("_FiltroCotizacion");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FiltroCotizacion(mCotizacion cotizacion)
        {
            List<mCotizacion> Cotizaciones = new List<mCotizacion>();
            try
            {
                Cotizaciones = _svc.GetCotizaciones(cotizacion);
                return PartialView("_ListCotizaciones", Cotizaciones);
            }
            catch (Exception)
            {
                ViewBag.Error = "Disculpe, pero ha ocurrido un error cargando las cotizaciones";
                return PartialView("_ListCotizaciones", Cotizaciones);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCotizacion(mCotizacion cotizacion)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _svc.InsertCotizacion(cotizacion);
                    return RedirectToAction("ListCotizaciones");
                }
                catch (Exception e)
                {
                    return Json(new { Satisfactorio = false, Msj = "Disculpe, pero los datos no pudieron ser guardados.", MsjError = e.Message });
                }
            }
            else
            {
                return Json(new { Satisfactorio = false, Msj = "Disculpe, pero debe de llenar todos los campos." });
            }
        }


    }
}