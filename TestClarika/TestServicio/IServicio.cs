﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TestModel;

namespace TestServicio
{
    [ServiceContract]
    public interface IServicio
    {
        [OperationContract]
        void InsertCotizacion(mCotizacion Cotizacion);

        [OperationContract]
        List<mCotizacion> GetCotizaciones(mCotizacion Cotizacion);
    }
}
