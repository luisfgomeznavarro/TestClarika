﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TestAcceso;
using TestModel;

namespace TestServicio
{
    public class Servicio : IServicio
    {
        ADCotizacion ADCotizacion = new ADCotizacion();
        public List<mCotizacion> GetCotizaciones(mCotizacion Cotizacion)
        {
            return ADCotizacion.GetCotizaciones(Cotizacion);
        }

        public void InsertCotizacion(mCotizacion Cotizacion)
        {
            ADCotizacion.InsertCotizacion(Cotizacion);
        }
    }
}
