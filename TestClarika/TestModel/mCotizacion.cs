﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestModel
{
    [Table("Cotizaciones", Schema = "dbo")]
    public class mCotizacion
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public string Cliente { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public string TipoSeguro { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public string FormaPago { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public DateTime FechaVencimiento { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public DateTime FechaCotizacion { get; set; }
        public bool Activa { get; set; }
        [Required(ErrorMessage = "El campo no puede ser vacío")]
        public string NumeroPoliza { get; set; }
    }
}
